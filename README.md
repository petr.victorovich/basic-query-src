# Basic Query Cookbook Source Files

Source files for the [Basic Querying in SuiteScript](https://stoic.software/cookbooks/)
cookbook, by [Eric T Grubaugh](https://stoic.software/).